def file_shuffler
  puts "Please enter a file name for shuffling"
  file_name = gets.chomp
  base_name = File.basename(file_name, ".*")
  extension = File.extname(file_name)
  
  File.open("#{base_name}-shuffled.txt", "w") do |f|
    File.readlines(file_name).shuffle.each do |line|
      f.puts line.chomp
    end
  end
end


if __FILE__ == $PROGRAM_NAME
  file_shuffler
end
